from slackclient import SlackClient
import fleetup
from slack import Slackbot

import logging
import sys

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

# create a file handler
# handler = logging.FileHandler('debug.log')
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)

# create a logging format
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)

# add the handlers to the logger
logger.addHandler(handler)

from config import SLACK_BOT_TOKEN, SLACK_BOT_ID

slack = SlackClient(SLACK_BOT_TOKEN)
AT_BOT = '<@{}>'.format(SLACK_BOT_ID)

bot = Slackbot(slack, logger)


@bot.command('ping', help="Check boss presence")
def ping(channel, arg):
    if arg:
        message = arg
    else:
        message = "Pong"

    return bot.post_message(channel, message)


@bot.command('help', help='Shows list of supported commands.')
def help(channel, arg):
    if arg is None:
        message = 'Supported commands: *{}*.\n*help _command_* for detailed help.'.format(
            ', '.join(bot._commands.keys()))
    elif arg in bot._command_help:
        message = bot._command_help[arg]
    else:
        message = 'No help entry found for {}'.format(arg)

    return bot.post_message(channel, message)


@bot.command('doctrines', help='Show all available doctrines')
def doctrines(channel, arg):
    doctrines = fleetup.get_doctrines()
    bot.post_message(channel, '\n'.join(doctrines.keys()))


@bot.command('doctrine', help='Show fitting names for a doctrine. Usage: *doctrine _name_*')
def doctrine(channel, arg):
    if arg:
        message, fittings = fleetup.get_fittings(doctrine=arg)
        message = '\n'.join([message] + list(fittings.keys()))
    else:
        message = "No doctrine name supplied. Usage: *doctrine _name_*"

    return bot.post_message(channel, message)


@bot.command('fittings', help='Show all available fittings')
def fittings(channel, arg):
    fittings = fleetup.get_fittings()
    if arg:
        filtered = [f for f in fittings.keys() if all(w in f.lower() for w in arg.lower().split())]
        if len(filtered) == 0:
            message = 'No fittings found for keywords {}'.format(arg)
        else:
            message = '\n'.join(filtered)
    else:
        message = '\n'.join(fittings.keys())

    return bot.post_message(channel, message)


@bot.command('fitting', help='Show fitting details. Usage: *fitting _name_*')
def fitting(channel, arg):
    if arg:
        fitting = fleetup.get_fitting(arg)
        message = fitting
    else:
        message = "No fitting name supplied. Usage: *fitting _name_*"

    return bot.post_message(channel, message)


if __name__ == '__main__':
    bot.run()
